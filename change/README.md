# Troco

Determine o menor número de moedas que devem ser devolvidas para um cliente
como troco, dado um valor a pagar, e o valor recebido.

- Lembre-se de tratar casos que possam causar algum erro

## Exemplos

- Crie uma função retornaTroco(valorTotal : float, valorRecebido : float);

- Uma chamada retornaTroco(15.00, 20.35) deverá imprimir:
  5 moedas de 1 real;
  1 moeda de 25 centavos;
  1 moeda de 10 centavos;

- Uma chamada retornaTroco(15.00, 15.66) deverá imprimir:
  1 moeda de 50 centavos;
  1 moeda de 10 centavos;
  1 moeda de 5 centavos;
  1 moeda de 1 centavo;